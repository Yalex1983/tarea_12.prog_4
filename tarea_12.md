# Entrega Tarea 12 Javascript
# EJERCICIOS OBJECTOS
## Ejercicio 1.js
1. Crear un metodo constructor llamada persona. Sus atributos son: nombre, edad y cedula. Construye los siguientes métodos para la clase:   
1.1 mostrar(): Muestra los datos de la persona.   
1.2 es_mayor_de_edad(): Devuelve un valor lógico indicando si es mayor de edad.
```javascript
function persona() {
    this.nombre = prompt("Ingrese el nombre: ");
    this.edad = parseInt(prompt("Ingrese la edad: "));
    this.cedula = prompt("Ingrese la cedula: ");
// Método mostrar(): Muestra los datos de la persona.
    this.mostrar = function () {
        return `Persona ${this.nombre} ${this.edad} ${this.cedula}`;  
    }
//Método es_mayor_de_edad(): Devuelve un valor lógico indicando si es mayor de edad. 
    this.es_mayor_de_edad = function () {
        if(this.edad >= 18){
            return true;
        }

        else{
            return false;
        }       
    }
}
```
## Ejercicio_2.js
Crea un metodo constructor llamado cuenta que tendrá los siguientes atributos: titular (que es nombre de la persona) y cantidad. El titular será obligatorio y la cantidad es opcional. Construye los siguientes métodos para el metodo:    
2.1 mostrar(): Muestra los datos de la cuenta.    
2.2 ingresar(cantidad): se ingresa una cantidad a la cuenta, si la cantidad introducida es negativa, no se hará nada.  
2.3 retirar(cantidad): se retira una cantidad a la cuenta. La cuenta puede estar en números rojos.
```javascript
function cuenta(){
    this.titular = prompt("Ingrese el nombre del titular de la cuenta: ");
    this.cuenta = 0;
// Método mostrar(): Muestra los datos de la cuenta. 
    this.mostrar = function (){
        if(this.titular !== null){
            return `Titular ${this.titular} Cuenta ${this.cuenta}`;
        }

        else{
            return "La cuenta no está creada";
        }
    }
// Método ingresar(cantidad): se ingresa una cantidad a la cuenta
    this.ingresar = function (){
        this.cantidad = parseFloat(prompt("Ingrese la cantidad a consignar: "));
        if(this.cantidad < 0){
            this.cuenta = this.cuenta;
        }
        else{
            this.cuenta = this.cuenta + this.cantidad
        }
        return this.mostrar();
    
    }
//Método retirar(cantidad): se retira una cantidad a la cuenta.
    this.retirar = function () {
        this.cantidad = parseFloat(prompt("Ingrese la cantidad a retirar: "));
        this.cuenta = this.cuenta - this.cantidad;
    return this.mostrar();
        
    }
}
```
## Ejercicio_3.js
Crear un metodo constructor llamado formulas. Construye los siguiente metodos para la clase:  

3.1 sumar(entero, entero)   
3.2 fibonacci(cantidad) a partir de una entero sacar los numeros  
3.3 operacion_modulo(cantidad) a partir de una cantidad mostrar cuales dan residuo 0   
3.4 primos(cantidad) a partir de una cantidad mostrar cuales son numeros primos
```javascript
function formulas() {
// Método sumar(entero, entero)
    this.suma = function () {
        this.num1 = parseInt(prompt("Ingrese un número entero: "));
        this.num2 = parseInt(prompt("Ingrese otro número entero: "));
        this.suma = this.num1 + this.num2;    
    }
// Método serie fibonacci(cantidad) a partir de una entero sacar los numeros 
    this.fibonacci = function () {
        this.cantidad = parseFloat(prompt(" Ingrese el número a calcular: "));
        this.fibo = [0,1];
        
        for (i = 2; i<= this.cantidad; i++)
        {
            this.fibo.push(this.fibo[i-1] + this.fibo[i-2]);
        }
        return this.fibo;
    }
// Método operación_modulo(cantidad) a partir de una cantidad mostrar cuales dan residuo 0 
    this.modulo = function () {
        this.cantidad = parseFloat(prompt("Ingrese un número: "));
        this,contResiduo = [];

        for(i = 0; i <= this.cantidad; i++){
            if(i%2===0){
                this.contResiduo.push(i);
            }
        }

        return this.contResiduo;
    }
// Método primos(cantidad) a partir de una cantidad mostrar cuales son numeros primos
    this.esPrimo = function (numero) {
        if (numero ==0 || numero == 1 || numero == 4) return false;

        for (i = 2; i < numero/2; i++){
            return false;
        }

        return true;
    }

    this.primoCant = function () {
        this.cantidad = parseInt(prompt("Ingrese un número entero: "));
        this.primo =[];

        for (i = 0; i <= this.cantidad; i++){
            if (this.esPrimo(i) === true){
                this.primo.push(i);
            }
            return this.primo;
        }
    }   
}
```
## Ejercicio_4
4 Crear un metodo constructor llamado persona. Sus atributos son: nombre, edad, DNI, sexo (H hombre, M mujer), peso y altura. Construye los siguiente metodos para la clase:

4.1 calcularIMC(): calculara si la persona esta en su peso ideal (peso en kg/(altura^2 en m)), si esta fórmula devuelve un valor menor que 20, la función devuelve un -1, si devuelve un número entre 20 y 25 (incluidos), significa que esta por debajo de su peso ideal la función devuelve un 0 y si devuelve un valor mayor que 25 significa que tiene sobrepeso, la función devuelve un 1. Te recomiendo que uses constantes para devolver estos valores.   
4.2 esMayorDeEdad(): indica si es mayor de edad, devuelve un booleano.   
4.3 comprobarSexo(char sexo): comprueba que el sexo introducido es correcto. Si no es correcto, sera H.
```javascript
function persona() {
    this.nombre = prompt("Ingrese el nombre de la persona: ");
    this.edad = parseInt(prompt("Ingrese la edad: "));
    this.dni = prompt("Ingrese el N° de DNI: ");
    this.sexo = prompt("Ingrese el sexo (Hombre / Mujer)");
    this. peso;
    this.altura;
// Método calcularIMC(): calculara si la persona esta en su peso ideal
    this.calcularIMC = function () {
        this.peso = parseFloat(prompt("Ingrese el peso en Kg: "));
        this.altura = parseFloat(prompt("Ingrese la altura en mts: "));

        if (this.peso/(this.altura^2) < 20){
            return -1
            if (this.peso/(this.altura^2) >= 20 && this.peso/(this.altura^2) <= 25){
                return 0
            }
        }
        else{
            return 1;
        }
    }
// Método esMayorDeEdad(): indica si es mayor de edad, devuelve un booleano.
    this.esMayorDeEdad = function () {
        if (this.edad >= 18){
            return true;
        }
        else{
            return false;
        }
    }
// Método comprobarSexo(char sexo): comprueba que el sexo introducido es correcto.
    this.comprobarSexo = function () {
        if(this.sexo.toUppercase().charAt(0) === 'H' || this.sexo.toUppercase().charAt(0) === 'M'){
           return this.sexo.toUppercase().charAt(0) 
        }
        return 'H'    
    }  
}
```

